package tech.pithy.gdcr.client

import groovyx.net.http.RESTClient

class GDCRClient {

    RESTClient restClient

    String gdcrUniverseUrl
    String cellId

    GDCRClient(RESTClient aRESTClient) {
        restClient = aRESTClient
    }

    String registerWithUniverse() {
        cellId = restClient.get(
            [
                path: '/universe/@currentUniverse/register'
            ]
        ).toString()
        return cellId
    }
}
