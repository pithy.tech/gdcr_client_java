package tech.pithy.gdcr.client

import groovyx.net.http.RESTClient
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNull
import static org.mockito.ArgumentMatchers.any
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class GDCRClientTest {

    public static final String SOME_CELL_COORDINATES = 'B4'
    GDCRClient clientUt

    RESTClient mockRestClient

    @Before
    void setUp() {
        mockRestClient = mock(RESTClient)
        clientUt = new GDCRClient(mockRestClient)
    }

    @Test
    void testClientCanRegisterWithTheServer() {
        when(mockRestClient.get(any(Map))).thenReturn(SOME_CELL_COORDINATES)
        assertEquals(SOME_CELL_COORDINATES, clientUt.registerWithUniverse() )
    }

    @Test
    void testClientCanRememberMyCellCoordinates() {
        assertNull(clientUt.cellId)

        when(mockRestClient.get(any(Map))).thenReturn(SOME_CELL_COORDINATES)
        clientUt.registerWithUniverse()

        assertEquals(SOME_CELL_COORDINATES, clientUt.cellId)
    }

}